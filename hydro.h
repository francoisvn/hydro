/*
  ------------------------------------------
  hydro header file
  ------------------------------------------
*/

#ifndef HYDRO_H
#define HYDRO_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include "SDL.h"
#include "SDL_thread.h"
#include "sge.h"
#include "SDL_ttf.h"
#include "SDL_image.h"

#define WIDTH 15
#define HEIGHT 10

#define WATER_MAX_VOL 10
#define WATER_PRESSURE_INC 1

#define DIR_U 0
#define DIR_R 1
#define DIR_D 2
#define DIR_L 3

#define MODE_PLAY 0
#define MODE_EDIT 1

#define BLOCK_SIZE 24
#define BLOCK_CAT_EMPTY 0
#define BLOCK_CAT_WALL 1
#define BLOCK_CAT_POWER 2
#define BLOCK_CAT_GOAL 3
#define BLOCK_WATER_ALPHA 100
#define BLOCK_WATER_ALPHA_MAX 230
#define BLOCK_EMPTY_FILENAME "gfx/empty.png"
#define BLOCK_EMPTY_TYPE 0
#define BLOCK_WALL_FILENAME "gfx/wall.png"
#define BLOCK_WALL_TYPE 1
#define BLOCK_HOLE_FILENAME "gfx/hole.png"
#define BLOCK_HOLE_TYPE 2
#define BLOCK_VENT_FILENAME "gfx/vent.png"
#define BLOCK_VENT_TYPE 3
#define BLOCK_PUMP_L_FILENAME "gfx/pump_l.png"
#define BLOCK_PUMP_R_FILENAME "gfx/pump_r.png"
#define BLOCK_PUMP_TYPE 4
#define BLOCK_POWER_0_FILENAME "gfx/power_0.png"
#define BLOCK_POWER_1_FILENAME "gfx/power_1.png"
#define BLOCK_POWER_TYPE 5
#define BLOCK_BATTERY_FILENAME "gfx/battery.png"
#define BLOCK_BATTERY_TYPE 6
#define BLOCK_VALVE_0_FILENAME "gfx/valve_0.png"
#define BLOCK_VALVE_1_FILENAME "gfx/valve_1.png"
#define BLOCK_VALVE_TYPE 7
#define BLOCK_LEVEL_0_FILENAME "gfx/level_0.png"
#define BLOCK_LEVEL_1_FILENAME "gfx/level_1.png"
#define BLOCK_LEVEL_TYPE 8
#define BLOCK_TURBINE_FILENAME "gfx/turbine.png"
#define BLOCK_TURBINE_TYPE 9
#define BLOCK_GOAL_0_FILENAME "gfx/goal_0.png"
#define BLOCK_GOAL_1_FILENAME "gfx/goal_1.png"
#define BLOCK_GOAL_TYPE 10
#define BLOCK_NOEDIT_FILENAME "gfx/noedit.png"
#define BLOCK_NOEDIT_ALPHA 80

struct BLOCK
{
  int type;
  int cat;
  int fillable;
  float vol;
  float initvol;
  int ventaccess;
  float rate;
  int dir;
  int conducting;
  int power;
  int normallyopen;
  int editable;
};
typedef struct BLOCK BLOCK;

Uint32 GetPixel(SDL_Surface *surf, int x, int y);
void DrawPixel(SDL_Surface *screen,int x,int y,Uint8 R,Uint8 G,Uint8 B);
void Slock(SDL_Surface *screen);
void Sulock(SDL_Surface *screen);
void apply_surface( int x, int y, SDL_Surface* source, SDL_Surface* destination );

void clean_up();

Uint32 GetPixel(SDL_Surface *surf, int x, int y)
{
  int bpp = surf->format->BytesPerPixel;
  Uint8 *p = (Uint8 *)surf->pixels + y * surf->pitch + x * bpp;

  switch (bpp)
  {
    case 1:
      return *p;

    case 2:
      return *(Uint16 *)p;

    case 3:
      if (SDL_BYTEORDER == SDL_BIG_ENDIAN)
        return p[0] << 16 | p[1] << 8 | p[2];
      else
        return p[0] | p[1] << 8 | p[2] << 16;

    case 4:
      return *(Uint32 *)p;

    default:
      return 0;
  }
}

void DrawPixel(SDL_Surface *screen,int x,int y,Uint8 R,Uint8 G,Uint8 B)
{
  Uint32 color = SDL_MapRGB(screen->format, R, G, B);
  switch (screen->format->BytesPerPixel)
  {
    case 1:
      {
        Uint8 *bufp;
        bufp = (Uint8 *)screen->pixels + y*screen->pitch + x;
        *bufp = color;
      }
      break;
    case 2:
      {
        Uint16 *bufp;
        bufp = (Uint16 *)screen->pixels + y*screen->pitch/2 + x;
        *bufp = color;
      }
      break;
    case 3:
      {
        Uint8 *bufp;
        bufp = (Uint8 *)screen->pixels + y*screen->pitch + x * 3;
        if(SDL_BYTEORDER == SDL_LIL_ENDIAN)
        {
          bufp[0] = color;
          bufp[1] = color >> 8;
          bufp[2] = color >> 16;
        }
        else
        {
          bufp[2] = color;
          bufp[1] = color >> 8;
          bufp[0] = color >> 16;
        }
      }
      break;
    case 4:
      {
        Uint32 *bufp;
        bufp = (Uint32 *)screen->pixels + y*screen->pitch/4 + x;
        *bufp = color;
      }
      break;
  }
}

void Slock(SDL_Surface *screen)
{
  if (SDL_MUSTLOCK(screen))
  {
    if (SDL_LockSurface(screen)<0)
    {
      return;
    }
  }
}

void Sulock(SDL_Surface *screen)
{
  if (SDL_MUSTLOCK(screen))
  {
    SDL_UnlockSurface(screen);
  }
}

void apply_surface( int x, int y, SDL_Surface* source, SDL_Surface* destination )
{
  SDL_Rect offset;
  
  offset.x = x;
  offset.y = y;
  
  SDL_BlitSurface( source, NULL, destination, &offset );
}

#endif
