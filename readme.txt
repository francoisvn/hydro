
blocks:
  empty     allows water to flow
  wall      barrier for water. does not conduct power
  power     barrier for water. conducts power
  hole      outlet for water. allows air flow
  vent      allows air flow
  battery   power source
  pump      pumps water in one direction using power
  valve     allows water flow depending on power
  level     allows power flow depending on surrounding water
  turbine   generates power if water can flow downwards through it
  goal      must get water to touch all these to win
  noedit    this toggles the editability of blocks in edit mode
  
keys:
  space   run/stop
  m       toggle mode (play/edit)
  n       new level (only in edit mode)
  l       load level
  s       save level (only in edit mode)
  r       randomize level (only in edit mode)
