/*
  ------------------------------------------
  hydro
  ------------------------------------------
  by francois van niekerk
  ------------------------------------------
*/

#include "hydro.h"

#define TITLE "hydro (alpha 0.05)"
#define MAX_FPS 30
#define DEBUG_VOL 0
#define DEBUG_VENT 0
#define DEBUG_POWER 0

#define LVL_FILE "level.dat"

#define TEXT_FONT "mgopenmodata.ttf"
#define TEXT_SIZE 12
#define TEXT_FPS_ACC 10

#ifndef EVENT_THREAD
  #define EVENT_THREAD 0
#endif

SDL_Surface *screen;
TTF_Font *font;
SDL_Surface	*block_empty;
SDL_Surface	*block_wall;
SDL_Surface	*block_hole;
SDL_Surface	*block_vent;
SDL_Surface	*block_pump_r;
SDL_Surface	*block_pump_l;
SDL_Surface	*block_power_0;
SDL_Surface	*block_power_1;
SDL_Surface	*block_battery;
SDL_Surface	*block_valve_0;
SDL_Surface	*block_valve_1;
SDL_Surface	*block_level_0;
SDL_Surface	*block_level_1;
SDL_Surface	*block_turbine;
SDL_Surface	*block_goal_0;
SDL_Surface	*block_goal_1;
SDL_Surface	*block_noedit;

int running;
int go;
int ticks;
int ticks_delta;
float fps, fps_hist[TEXT_FPS_ACC];

int mousex,mousey;

BLOCK level[WIDTH][HEIGHT];

int mode;
int selected_tool;

int get_block_cat(int type)
{
  switch (type)
  {
    case BLOCK_EMPTY_TYPE:
    case BLOCK_HOLE_TYPE:
    case BLOCK_VENT_TYPE:
      return BLOCK_CAT_EMPTY;
    case BLOCK_WALL_TYPE:
      return BLOCK_CAT_WALL;
    case BLOCK_PUMP_TYPE:
    case BLOCK_POWER_TYPE:
    case BLOCK_BATTERY_TYPE:
    case BLOCK_VALVE_TYPE:
    case BLOCK_LEVEL_TYPE:
    case BLOCK_TURBINE_TYPE:
      return BLOCK_CAT_POWER;
    case BLOCK_GOAL_TYPE:
      return BLOCK_CAT_GOAL;
    default:
      return -1;
  }
}

void power_spread(int i, int j)
{
  if (level[i][j].power)
    return;
  
  level[i][j].power=1;
  
  if ((i>0)&&(level[i-1][j].conducting))
    power_spread(i-1,j);
  if ((j>0)&&(level[i][j-1].conducting))
    power_spread(i,j-1);
  if ((i<(WIDTH-1))&&(level[i+1][j].conducting))
    power_spread(i+1,j);
  if ((j<(HEIGHT-1))&&(level[i][j+1].conducting))
    power_spread(i,j+1);
}

void vent_spread(int i, int j)
{
  if ((level[i][j].ventaccess)||(level[i][j].vol>1))
    return;
  
  level[i][j].ventaccess=1;
  
  if ((i>0)&&(level[i-1][j].fillable))
    vent_spread(i-1,j);
  if ((j>0)&&(level[i][j-1].fillable))
    vent_spread(i,j-1);
  if ((i<(WIDTH-1))&&(level[i+1][j].fillable))
    vent_spread(i+1,j);
  if ((j<(HEIGHT-1))&&(level[i][j+1].fillable))
    vent_spread(i,j+1);
}

float exchange_water(BLOCK *b1, BLOCK *b2, int dh)
{
  float diff;
  float d;
  
  if ((dh<0)&&(!b2->ventaccess)&&(b2->vol<1))
    return 0;
  
  diff=((b1->vol)-(b2->vol))+(WATER_PRESSURE_INC*dh);
  if (diff<0)
    return 0;
  
  if (b1->vol<diff/2)
  {
    d=b1->vol;
    b2->vol+=b1->vol;
    b1->vol=0;
  }
  else if (b2->vol+diff/2>WATER_MAX_VOL)
  {
    d=WATER_MAX_VOL-b2->vol;
    b1->vol-=WATER_MAX_VOL-b2->vol;
    b2->vol=WATER_MAX_VOL;
  }
  else
  {
    d=diff;
    b1->vol-=diff/2;
    b2->vol+=diff/2;
  }
  
  return d;
}

void do_water_flow_block(BLOCK *b, int i, int j)
{
  if ((j<(HEIGHT-1))&&(level[i][j+1].fillable))
    exchange_water(b,&level[i][j+1],1);
  if ((j>0)&&(level[i][j-1].fillable))
    exchange_water(b,&level[i][j-1],-1);
  if ((i>0)&&(level[i-1][j].fillable))
    exchange_water(b,&level[i-1][j],0);
  if ((i<(WIDTH-1))&&(level[i+1][j].fillable))
    exchange_water(b,&level[i+1][j],0);
}

void do_water_flow_down_block(BLOCK *b, int i, int j)
{
  if ((j<(HEIGHT-1))&&(level[i][j+1].fillable))
    exchange_water(b,&level[i][j+1],1);
}

void do_water_flow()
{
  int i,j;
  BLOCK *b;
  
  for (i=0;i<WIDTH;i++)
  {
    for (j=0;j<HEIGHT;j++)
    {
      b=&level[i][j];
      
      if (b->fillable)
        do_water_flow_block(b,i,j);
    }
  }
}

void do_water_flow_down()
{
  int i,j;
  BLOCK *b;
  
  for (i=0;i<WIDTH;i++)
  {
    for (j=0;j<HEIGHT;j++)
    {
      b=&level[i][j];
      
      if ((b->fillable)&&(b->vol<1))
        do_water_flow_down_block(b,i,j);
    }
  }
}

void do_vent_power()
{
  int i,j;
  BLOCK *b;
  
  for (i=0;i<WIDTH;i++)
  {
    for (j=0;j<HEIGHT;j++)
    {
      b=&level[i][j];
      b->ventaccess=0;
      if ((b->type!=BLOCK_GOAL_TYPE)&&(b->type!=BLOCK_TURBINE_TYPE))
        b->power=0;
    }
  }
  
  for (i=0;i<WIDTH;i++)
  {
    for (j=0;j<HEIGHT;j++)
    {
      b=&level[i][j];
      
      if (((b->type==BLOCK_VENT_TYPE)||(b->type==BLOCK_HOLE_TYPE))&&(b->vol<1))
        vent_spread(i,j);
      
      if (b->type==BLOCK_BATTERY_TYPE)
        power_spread(i,j);
      
      if ((b->type==BLOCK_TURBINE_TYPE)&&(b->power))
      {
        b->power=0;
        power_spread(i,j);
      }
    }
  }
}

int get_goal_total()
{
  int i,j;
  BLOCK *b;
  int s=0;
  
  for (i=0;i<WIDTH;i++)
  {
    for (j=0;j<HEIGHT;j++)
    {
      b=&level[i][j];
      if (b->type==BLOCK_GOAL_TYPE)
        s++;
    }
  }
  
  return s;
}

int get_goal_done()
{
  int i,j;
  BLOCK *b;
  int s=0;
  
  for (i=0;i<WIDTH;i++)
  {
    for (j=0;j<HEIGHT;j++)
    {
      b=&level[i][j];
      if ((b->type==BLOCK_GOAL_TYPE)&&(b->power))
        s++;
    }
  }
  
  return s;
}

int is_touching_water(int i, int j)
{
  if ((j<(HEIGHT-1))&&(level[i][j+1].vol>=1))
    return 1;
  else if ((j>0)&&(level[i][j-1].vol>0))
    return 1;
  else if ((i>0)&&(level[i-1][j].vol>0))
    return 1;
  else if ((i<(WIDTH-1))&&(level[i+1][j].vol>0))
    return 1;
  else
    return 0;
}

void do_special_blocks()
{
  int i,j;
  int d;
  int t;
  BLOCK *b,*b1,*b2;
  
  for (i=0;i<WIDTH;i++)
  {
    for (j=0;j<HEIGHT;j++)
    {
      b=&level[i][j];
      
      t=is_touching_water(i,j);
      
      switch(b->type)
      {
        case BLOCK_HOLE_TYPE:
          if (b->vol<WATER_MAX_VOL)
          {
            b->vol+=b->rate;
            if (b->vol>WATER_MAX_VOL)
              b->vol=WATER_MAX_VOL;
          }
          break;
        case BLOCK_VENT_TYPE:
          if (b->vol>0)
          {
            b->vol-=b->rate;
            if (b->vol<0)
              b->vol=0;
          }
          break;
        case BLOCK_PUMP_TYPE:
          if (b->power)
          {
            if (b->dir==DIR_R)
              d=+1;
            else
              d=-1;
            if ((((d>0)&&(i>0))||((d<0)&&(i<(WIDTH-1))))&&((level[i-d][j].fillable)||((level[i-d][j].type==BLOCK_PUMP_TYPE)&&(b->dir==level[i-d][j].dir))))
            {
              b1=&level[i-d][j];
              
              if ((b1->vol>0)&&(b->vol<WATER_MAX_VOL))
              {
                if (((b1->vol)>(b->rate))&&((b->vol+b->rate)<(WATER_MAX_VOL)))
                {
                  b1->vol-=b->rate;
                  b->vol+=b->rate;
                }
                else if ((b1->vol)>(WATER_MAX_VOL-b->vol))
                {
                  b1->vol-=WATER_MAX_VOL-b->vol;
                  b->vol=WATER_MAX_VOL;
                }
                else
                {
                  b->vol+=b1->vol;
                  b1->vol=0;
                }
              }
            }
            if ((b->vol>0)&&((((d<0)&&(i>0))||((d>0)&&(i<(WIDTH-1))))&&(level[i+d][j].fillable)))
            {
              b2=&level[i+d][j];
              exchange_water(b,b2,0);
            }
          }
          break;
        case BLOCK_VALVE_TYPE:
          if (b->power)
          {
            if (b->normallyopen)
              b->fillable=0;
            else
              b->fillable=1;
          }
          else
          {
            if (b->normallyopen)
              b->fillable=1;
            else
              b->fillable=0;
          }
          break;
        case BLOCK_LEVEL_TYPE: 
          if (t)
          {
            if (b->normallyopen)
              b->conducting=0;
            else
              b->conducting=1;
          }
          else
          {
            if (b->normallyopen)
              b->conducting=1;
            else
              b->conducting=0;
          }
          break;
        case BLOCK_TURBINE_TYPE:
          if (((j>0)&&(level[i][j-1].fillable))&&((j<(HEIGHT-1))&&(level[i][j+1].fillable)))
          {
            b1=&level[i][j-1];
            b2=&level[i][j+1];
            if (exchange_water(b1,b2,1)>0)
              b->power=1;
            else
              b->power=0;
          }
          else
            b->power=0;
          break;
        case BLOCK_GOAL_TYPE: 
          if (t)
            b->power=1;
          else
            b->power=0;
          break;
      }
    }
  }
}

void init_block(BLOCK *b)
{
  b->cat=get_block_cat(b->type);
  switch (b->type)
  {
    case BLOCK_EMPTY_TYPE:
      b->fillable=1;
      b->conducting=0;
      break;
    case BLOCK_WALL_TYPE:
      b->fillable=0;
      b->conducting=0;
      break;
    case BLOCK_POWER_TYPE:
      b->fillable=0;
      b->conducting=1;
      break;
    case BLOCK_HOLE_TYPE:
      b->fillable=1;
      b->conducting=0;
      b->rate=0.1;
      break;
    case BLOCK_VENT_TYPE:
      b->fillable=1;
      b->conducting=0;
      b->rate=0.5;
      break;
    case BLOCK_BATTERY_TYPE:
      b->fillable=0;
      b->conducting=1;
      break;
    case BLOCK_PUMP_TYPE:
      b->fillable=0;
      b->conducting=1;
      b->rate=0.1;
      break;
    case BLOCK_VALVE_TYPE:
      if (b->normallyopen)
        b->fillable=1;
      else
        b->fillable=0;
      b->conducting=1;
      break;
    case BLOCK_LEVEL_TYPE:
      b->fillable=0;
      if (b->normallyopen)
        b->conducting=1;
      else
        b->conducting=0;
      break;
    case BLOCK_TURBINE_TYPE:
      b->fillable=0;
      b->conducting=1;
      break;
    case BLOCK_GOAL_TYPE:
      b->fillable=0;
      b->conducting=0;
      b->power=0;
      break;
  }
  
  b->initvol=0;
  b->vol=0;
  b->ventaccess=0;
  b->power=0;
}

void new_level()
{
  int i,j;
  BLOCK *b;
  
  for (i=0;i<WIDTH;i++)
  {
    for (j=0;j<HEIGHT;j++)
    {
      b=&level[i][j];
      
      if ((i==0)||(i==(WIDTH-1))||(j==0)||(j==(HEIGHT-1)))
      {
        b->type=BLOCK_WALL_TYPE;
        init_block(b);
        b->editable=0;
      }
      else
      {
        b->type=BLOCK_EMPTY_TYPE;
        init_block(b);
        b->editable=1;
      }
    }
  }
}

void random_level()
{
  int i,j;
  BLOCK *b;
  int nt;
  
  for (i=0;i<WIDTH;i++)
  {
    for (j=0;j<HEIGHT;j++)
    {
      b=&level[i][j];
      
      if ((i==0)||(i==(WIDTH-1))||(j==0)||(j==(HEIGHT-1)))
      {
        b->type=BLOCK_WALL_TYPE;
        init_block(b);
        b->editable=0;
      }
      else
      {
        if (round(rand()/((double)RAND_MAX+1)*1.5)==0)
          nt=BLOCK_EMPTY_TYPE;
        else if (round(rand()/((double)RAND_MAX+1)*3)==0)
          nt=BLOCK_WALL_TYPE;
        else
          nt=rand()/((double)RAND_MAX+1)*(BLOCK_GOAL_TYPE+1);
        b->type=nt;
        
        init_block(b);
        b->editable=1;
        
        switch (b->type)
        {
          case BLOCK_PUMP_TYPE:
            if (round(rand()/((double)RAND_MAX+1)))
              b->dir=DIR_L;
            else
              b->dir=DIR_R;
            break;
          case BLOCK_VALVE_TYPE:
            if (round(rand()/((double)RAND_MAX+1)))
              b->normallyopen=1;
            else
              b->normallyopen=0;
            break;
          case BLOCK_LEVEL_TYPE:
            if (round(rand()/((double)RAND_MAX+1)))
              b->normallyopen=1;
            else
              b->normallyopen=0;
            break;
        }
      }
    }
  }
}

void reset_level()
{
  int i,j;
  BLOCK *b;
  int t;
  
  for (i=0;i<WIDTH;i++)
  {
    for (j=0;j<HEIGHT;j++)
    {
      b=&level[i][j];
      
      b->vol=b->initvol;
    }
  }
  
  for (i=0;i<WIDTH;i++)
  {
    for (j=0;j<HEIGHT;j++)
    {
      b=&level[i][j];
      
      t=is_touching_water(i,j);
      
      switch (b->type)
      {
        case BLOCK_VALVE_TYPE:
          if (b->power)
          {
            if (b->normallyopen)
              b->fillable=0;
            else
              b->fillable=1;
          }
          else
          {
            if (b->normallyopen)
              b->fillable=1;
            else
              b->fillable=0;
          }
          break;
        case BLOCK_LEVEL_TYPE: 
          if (t)
          {
            if (b->normallyopen)
              b->conducting=0;
            else
              b->conducting=1;
          }
          else
          {
            if (b->normallyopen)
              b->conducting=1;
            else
              b->conducting=0;
          }
          break;
        case BLOCK_GOAL_TYPE: 
          if (t)
            b->power=1;
          else
            b->power=0;
          break;
        case BLOCK_TURBINE_TYPE:
          b->power=0;
          break;
      }
    }
  }
  
  do_vent_power();
}

void do_cycle()
{
  if (go)
  {
    do_special_blocks();  
    do_water_flow();
    do_water_flow_down();
    do_vent_power();
  }
  else
    reset_level();
}

void load_level()
{
  FILE *fPtr;
  int ret;
  int s;
  int w,h;
  
  printf("loading level...");
  if ((fPtr=fopen(LVL_FILE,"rb"))==NULL)
  {
    printf("error: opening file\n");
    return;
  }
  
  ret=fread(&s,sizeof(s),1,fPtr);
  if (s!=sizeof(BLOCK))
  {
    printf("error: invalid version\n");
    return;
  }
  
  ret=fread(&w,sizeof(w),1,fPtr);
  ret=fread(&h,sizeof(h),1,fPtr);
  if ((w!=WIDTH)||(h!=HEIGHT))
  {
    printf("error: invalid size\n");
    return;
  }
  ret=fread(level,sizeof(BLOCK),WIDTH*HEIGHT,fPtr);
  
  fclose(fPtr);
  
  reset_level();
  printf("done\n");
}

void save_level()
{
  FILE *fPtr;
  int ret;
  int s;
  int w,h;
  
  printf("saving level...");
  if ((fPtr=fopen(LVL_FILE,"wb"))==NULL)
  {
    printf("error: opening file\n");
    return;
  }
  
  s=sizeof(BLOCK);
  w=WIDTH;
  h=HEIGHT;
  
  ret=fwrite(&s,sizeof(s),1,fPtr);
  ret=fwrite(&w,sizeof(w),1,fPtr);
  ret=fwrite(&h,sizeof(h),1,fPtr);
  ret=fwrite(level,sizeof(BLOCK),WIDTH*HEIGHT,fPtr);
  
  fclose(fPtr);
  printf("done\n");
}

void draw_main()
{
  SDL_Surface *message;
  SDL_Color textColor={0,0,0};
  Uint32 borderColor;
  Uint32 waterColor;
  char text[256];
  int i,j;
  BLOCK *b;
  SDL_Rect draw_rect;
  int h0,h1,h2,h3,h4;
  Sint16 trapx[4];
  Sint16 trapy[4];
  
  Slock(screen);
  sge_Update_OFF();
  
  SDL_FillRect(screen,NULL,SDL_MapRGB(screen->format,255,255,255));
  
  borderColor=SDL_MapRGB(screen->format,59,59,59);
  waterColor=SDL_MapRGB(screen->format,24,76,130);
  SDL_SetAlpha(block_noedit,SDL_SRCALPHA|SDL_RLEACCEL,BLOCK_NOEDIT_ALPHA);
  
  for (i=0;i<WIDTH;i++)
  {
    for (j=0;j<HEIGHT;j++)
    {
      b=&level[i][j];
      draw_rect.x=BLOCK_SIZE*i;
      draw_rect.y=BLOCK_SIZE*j;
      
      switch(b->type)
      {
        case BLOCK_EMPTY_TYPE:
          SDL_BlitSurface(block_empty,NULL,screen,&draw_rect);
          break;
        case BLOCK_HOLE_TYPE:
          SDL_BlitSurface(block_hole,NULL,screen,&draw_rect);
          break;
        case BLOCK_VENT_TYPE:
          SDL_BlitSurface(block_vent,NULL,screen,&draw_rect);
          break;
      }
      
      if (b->vol>0)
      {
        if ((b->vol<1)||((j>0)&&(level[i][j-1].fillable)&&(level[i][j-1].vol==0)))
        {
          if (b->vol>1)
            h2=BLOCK_SIZE*(j);
          else
            h2=BLOCK_SIZE*(j+1)-BLOCK_SIZE*b->vol;
          
          if ((i>0)&&(level[i-1][j].fillable))
          {
            if (level[i-1][j].vol>=1)
            {
              h1=BLOCK_SIZE*(j);
            }
            else if (level[i-1][j].vol==0)
              h1=BLOCK_SIZE*(j+1);
            else
            {
              h0=BLOCK_SIZE*(j+1)-BLOCK_SIZE*level[i-1][j].vol;
              h1=ceil((float)(h2+h0)/2);
            }
          }
          else
            h1=h2;
          if ((i<(WIDTH-1))&&(level[i+1][j].fillable))
          {
            if (level[i+1][j].vol>=1)
            {
              h3=BLOCK_SIZE*(j);
            }
            else if (level[i+1][j].vol==0)
              h3=BLOCK_SIZE*(j+1);
            else
            {
              h4=BLOCK_SIZE*(j+1)-BLOCK_SIZE*level[i+1][j].vol;
              h3=ceil((float)(h2+h4)/2);
            }
          }
          else
            h3=h2;
          
          h1--;h2--;h3--;
          
          trapx[0]=draw_rect.x;
          trapy[0]=h1;
          trapx[1]=draw_rect.x+BLOCK_SIZE/2;
          trapy[1]=h2;
          trapx[2]=draw_rect.x+BLOCK_SIZE/2;
          trapy[2]=BLOCK_SIZE*(j+1);
          trapx[3]=draw_rect.x;
          trapy[3]=BLOCK_SIZE*(j+1);
          sge_FilledPolygonAlpha(screen,4,trapx,trapy,waterColor,BLOCK_WATER_ALPHA+(BLOCK_WATER_ALPHA_MAX-BLOCK_WATER_ALPHA)*(b->vol/WATER_MAX_VOL));
          
          trapx[0]=draw_rect.x+BLOCK_SIZE/2+1;
          trapy[0]=h2;
          trapx[1]=draw_rect.x+BLOCK_SIZE;
          trapy[1]=h3;
          trapx[2]=draw_rect.x+BLOCK_SIZE;
          trapy[2]=BLOCK_SIZE*(j+1);
          trapx[3]=draw_rect.x+BLOCK_SIZE/2+1;
          trapy[3]=BLOCK_SIZE*(j+1);
          sge_FilledPolygonAlpha(screen,4,trapx,trapy,waterColor,BLOCK_WATER_ALPHA+(BLOCK_WATER_ALPHA_MAX-BLOCK_WATER_ALPHA)*(b->vol/WATER_MAX_VOL));
          
          sge_AALine(screen,draw_rect.x,h1,draw_rect.x+BLOCK_SIZE/2,h2,borderColor);
          sge_AALine(screen,draw_rect.x+BLOCK_SIZE/2,h2,draw_rect.x+BLOCK_SIZE,h3,borderColor);
        }
        else
        {
          sge_FilledRectAlpha(screen,draw_rect.x,draw_rect.y,draw_rect.x+BLOCK_SIZE,draw_rect.y+BLOCK_SIZE,waterColor,BLOCK_WATER_ALPHA+(BLOCK_WATER_ALPHA_MAX-BLOCK_WATER_ALPHA)*(b->vol/WATER_MAX_VOL));
        }
        draw_rect.y=BLOCK_SIZE*j;
      }
      
      switch(b->type)
      {
        case BLOCK_WALL_TYPE:
          SDL_BlitSurface(block_wall,NULL,screen,&draw_rect);
          break;
        case BLOCK_PUMP_TYPE:
          if (b->dir==DIR_R)
            SDL_BlitSurface(block_pump_r,NULL,screen,&draw_rect);
          else
            SDL_BlitSurface(block_pump_l,NULL,screen,&draw_rect);
          break;
        case BLOCK_POWER_TYPE:
          if (b->power)
            SDL_BlitSurface(block_power_1,NULL,screen,&draw_rect);
          else
            SDL_BlitSurface(block_power_0,NULL,screen,&draw_rect);
          break;
        case BLOCK_BATTERY_TYPE:
          SDL_BlitSurface(block_battery,NULL,screen,&draw_rect);
          break;
        case BLOCK_VALVE_TYPE:
          if (b->fillable)
            SDL_BlitSurface(block_valve_1,NULL,screen,&draw_rect);
          else
            SDL_BlitSurface(block_valve_0,NULL,screen,&draw_rect);
          break;
        case BLOCK_LEVEL_TYPE:
          if (b->conducting)
            SDL_BlitSurface(block_level_1,NULL,screen,&draw_rect);
          else
            SDL_BlitSurface(block_level_0,NULL,screen,&draw_rect);
          break;
        case BLOCK_TURBINE_TYPE:
          SDL_BlitSurface(block_turbine,NULL,screen,&draw_rect);
          break;
        case BLOCK_GOAL_TYPE:
          if (b->power)
            SDL_BlitSurface(block_goal_1,NULL,screen,&draw_rect);
          else
            SDL_BlitSurface(block_goal_0,NULL,screen,&draw_rect);
          break;
      }
      
      if ((i>0)&&(level[i-1][j].cat!=b->cat))
        sge_Line(screen,draw_rect.x,draw_rect.y,draw_rect.x,draw_rect.y+BLOCK_SIZE,borderColor);
      if ((j>0)&&(level[i][j-1].cat!=b->cat))
        sge_Line(screen,draw_rect.x,draw_rect.y,draw_rect.x+BLOCK_SIZE,draw_rect.y,borderColor);
      if ((i<(WIDTH-1))&&(level[i+1][j].cat!=b->cat))
        sge_Line(screen,draw_rect.x+BLOCK_SIZE-1,draw_rect.y,draw_rect.x+BLOCK_SIZE-1,draw_rect.y+BLOCK_SIZE,borderColor);
      if ((j<(HEIGHT-1))&&(level[i][j+1].cat!=b->cat))
        sge_Line(screen,draw_rect.x,draw_rect.y+BLOCK_SIZE-1,draw_rect.x+BLOCK_SIZE,draw_rect.y+BLOCK_SIZE-1,borderColor);
      
      if ((!go)&&(!b->editable))
        SDL_BlitSurface(block_noedit,NULL,screen,&draw_rect);
      
      #if DEBUG_VOL
        if (b->vol>0)
        {
          sprintf(text,"%.2f",b->vol);
          message=TTF_RenderText_Blended(font, text, textColor );
          apply_surface(draw_rect.x, draw_rect.y, message, screen );
          SDL_FreeSurface(message);
        }
      #endif
      #if DEBUG_VENT
        if (b->fillable)
        {
          sprintf(text,"%d %.0f",b->ventaccess,b->vol);
          message=TTF_RenderText_Blended(font, text, textColor );
          apply_surface(draw_rect.x, draw_rect.y, message, screen );
          SDL_FreeSurface(message);
        }
      #endif
      #if DEBUG_POWER
        if (b->conducting)
        {
          sprintf(text,"%d",b->power);
          message=TTF_RenderText_Blended(font, text, textColor );
          apply_surface(draw_rect.x, draw_rect.y, message, screen );
          SDL_FreeSurface(message);
        }
      #endif
    }
  }
  
  draw_rect.x=0;
  draw_rect.y=HEIGHT*BLOCK_SIZE+5;
  SDL_BlitSurface(block_empty,NULL,screen,&draw_rect);
  draw_rect.x+=BLOCK_SIZE;
  SDL_BlitSurface(block_wall,NULL,screen,&draw_rect);
  draw_rect.x+=BLOCK_SIZE;
  SDL_BlitSurface(block_power_0,NULL,screen,&draw_rect);
  draw_rect.x+=BLOCK_SIZE;
  SDL_BlitSurface(block_hole,NULL,screen,&draw_rect);
  draw_rect.x+=BLOCK_SIZE;
  SDL_BlitSurface(block_vent,NULL,screen,&draw_rect);
  draw_rect.x+=BLOCK_SIZE;
  SDL_BlitSurface(block_battery,NULL,screen,&draw_rect);
  draw_rect.x+=BLOCK_SIZE;
  SDL_BlitSurface(block_pump_r,NULL,screen,&draw_rect);
  draw_rect.x+=BLOCK_SIZE;
  SDL_BlitSurface(block_valve_1,NULL,screen,&draw_rect);
  draw_rect.x+=BLOCK_SIZE;
  SDL_BlitSurface(block_level_0,NULL,screen,&draw_rect);
  draw_rect.x+=BLOCK_SIZE;
  SDL_BlitSurface(block_turbine,NULL,screen,&draw_rect);
  draw_rect.x+=BLOCK_SIZE;
  SDL_BlitSurface(block_goal_1,NULL,screen,&draw_rect);
  draw_rect.x+=BLOCK_SIZE;
  SDL_SetAlpha(block_noedit,0,255);
  SDL_BlitSurface(block_noedit,NULL,screen,&draw_rect);
  
  sprintf(text,"X");
  message=TTF_RenderText_Blended(font, text, textColor );
  apply_surface(selected_tool*BLOCK_SIZE, draw_rect.y, message, screen );
  SDL_FreeSurface(message);
  
  sprintf(text,"m:(%02d,%02d)(%03d,%03d) fps:%.1f go:%d goal:%d/%d",(mousex/BLOCK_SIZE),(mousey/BLOCK_SIZE),mousex,mousey,fps,go,get_goal_done(),get_goal_total());
  if (mode==MODE_PLAY)
    sprintf(text+strlen(text)," mode:play");
  else
    sprintf(text+strlen(text)," mode:edit");
  message=TTF_RenderText_Blended(font, text, textColor );
  apply_surface(5, screen->h-TEXT_SIZE-7, message, screen );
  SDL_FreeSurface(message);
  
  sge_Update_ON();
  Sulock(screen);
  SDL_Flip(screen);
}

void event_handler(SDL_Event event)
{
  SDLKey key;
  int x,y;
  int newtype;
  BLOCK *b;
  int g;
  
  switch (event.type)
  {
    case SDL_KEYDOWN:
      key=event.key.keysym.sym;
      switch (key)
      {
        case SDLK_SPACE:
          go=!go;
          reset_level();
          break;
        case SDLK_n:
          if (mode==MODE_EDIT)
          {
            go=0;
            new_level();
          }
          break;
        case SDLK_m:
          if (mode==MODE_PLAY)
            mode=MODE_EDIT;
          else
            mode=MODE_PLAY;
          break;
        case SDLK_l:
          go=0;
          load_level();
          break;
        case SDLK_r:
          if (mode==MODE_EDIT)
          {
            go=0;
            random_level();
          }
          break;
        case SDLK_s:
          if (mode==MODE_EDIT)
          {
            g=go;
            go=0;
            save_level();
            go=g;
          }
          break;
        case SDLK_ESCAPE:
          running=0;
          break;
        default:
          break;
      }
      break;
    case SDL_MOUSEMOTION:
      mousex=event.motion.x;
      mousey=event.motion.y;
      break;
    case SDL_MOUSEBUTTONDOWN:
      mousex=event.button.x;
      mousey=event.button.y;
      if (event.button.button==SDL_BUTTON_LEFT)
      {
        if (mousey>(BLOCK_SIZE*HEIGHT))
        {
          if ((mousex/BLOCK_SIZE)<=11)
            selected_tool=mousex/BLOCK_SIZE;
        }
        else if (!go)
        {
          x=mousex/BLOCK_SIZE;
          y=mousey/BLOCK_SIZE;
          b=&level[x][y];
          
          if ((mode==MODE_EDIT)||(b->editable))
          {
            switch (selected_tool)
            {
              case 0:
                newtype=BLOCK_EMPTY_TYPE;
                break;
              case 1:
                newtype=BLOCK_WALL_TYPE;
                break;
              case 2:
                newtype=BLOCK_POWER_TYPE;
                break;
              case 3:
                newtype=BLOCK_HOLE_TYPE;
                break;
              case 4:
                newtype=BLOCK_VENT_TYPE;
                break;
              case 5:
                newtype=BLOCK_BATTERY_TYPE;
                break;
              case 6:
                newtype=BLOCK_PUMP_TYPE;
                if (b->type==BLOCK_PUMP_TYPE)
                {
                  if (b->dir==DIR_L)
                    b->dir=DIR_R;
                  else
                    b->dir=DIR_L;
                }
                else
                  b->dir=DIR_R;
                break;
              case 7:
                newtype=BLOCK_VALVE_TYPE;
                if (b->type==BLOCK_VALVE_TYPE)
                  b->normallyopen=!b->normallyopen;
                else
                  b->normallyopen=1;
                break;
              case 8:
                newtype=BLOCK_LEVEL_TYPE;
                if (b->type==BLOCK_LEVEL_TYPE)
                  b->normallyopen=!b->normallyopen;
                else
                  b->normallyopen=0;
                break;
              case 9:
                newtype=BLOCK_TURBINE_TYPE;
                break;
              case 10:
                newtype=BLOCK_GOAL_TYPE;
                break;
              case 11:
                newtype=-1;
                if (mode==MODE_EDIT)
                  b->editable=!b->editable;
                break;
              default:
                newtype=BLOCK_EMPTY_TYPE;
                break;
            }
            
            if (newtype!=-1)
            {
              b->type=newtype;
              init_block(b);
            }
          }
        }
      }
      break;
    case SDL_QUIT:
      running=0;
      break;
  }
}

#if EVENT_THREAD
int event_handler_thread(void *data)
{
  SDL_Event event;
  
  while (running)
  {
    if(SDL_PollEvent(&event))
    {
      event_handler(event);
    }
    else
    {
      SDL_Delay(1);
    }
  }
  
  return 0;
}
#endif

SDL_Surface *load_image(char *filename)
{
  SDL_Surface *tmp_image;
  SDL_Surface *ret_image;
  
  tmp_image = IMG_Load(filename);
	if(!tmp_image)
	{
		printf("Unable to open image: %s\n",filename);
    exit(1);
  }
	ret_image=SDL_DisplayFormat(tmp_image);
	SDL_FreeSurface(tmp_image);
	
	return ret_image;
}

void load_graphics()
{
  block_empty=load_image(BLOCK_EMPTY_FILENAME);
  block_wall=load_image(BLOCK_WALL_FILENAME);
  block_hole=load_image(BLOCK_HOLE_FILENAME);
  block_vent=load_image(BLOCK_VENT_FILENAME);
  block_pump_r=load_image(BLOCK_PUMP_R_FILENAME);
  block_pump_l=load_image(BLOCK_PUMP_L_FILENAME);
  block_power_0=load_image(BLOCK_POWER_0_FILENAME);
  block_power_1=load_image(BLOCK_POWER_1_FILENAME);
  block_battery=load_image(BLOCK_BATTERY_FILENAME);
	block_valve_0=load_image(BLOCK_VALVE_0_FILENAME);
  block_valve_1=load_image(BLOCK_VALVE_1_FILENAME);
  block_level_0=load_image(BLOCK_LEVEL_0_FILENAME);
  block_level_1=load_image(BLOCK_LEVEL_1_FILENAME);
  block_turbine=load_image(BLOCK_TURBINE_FILENAME);
  block_goal_0=load_image(BLOCK_GOAL_0_FILENAME);
  block_goal_1=load_image(BLOCK_GOAL_1_FILENAME);
  block_noedit=load_image(BLOCK_NOEDIT_FILENAME);
}

int main(int argc, char *argv[])
{
  #if EVENT_THREAD
    SDL_Thread *ev_thread;
  #else
    SDL_Event event;
  #endif
  int tprev=0;
  int i;
  float fps_avg;
  
  if (SDL_Init(SDL_INIT_TIMER|SDL_INIT_VIDEO)<0)
  {
    printf( "Unable to init SDL: %s\n", SDL_GetError());
    exit(1);
  }
  atexit(clean_up);
  
  if(TTF_Init()<0)
  {
    printf( "Unable to init TTF: %s\n", TTF_GetError());
    exit(1);
  }
  
  SDL_WM_SetCaption(TITLE,TITLE);
  
  Uint32 flags = SDL_SWSURFACE|SDL_DOUBLEBUF;
  //SDL_FULLSCREEN SDL_ASYNCBLIT SDL_OPENGL SDL_RESIZABLE
  screen = SDL_SetVideoMode(WIDTH*BLOCK_SIZE,(HEIGHT+1)*BLOCK_SIZE+30, 24, flags);
  if (screen == NULL)
  {
    printf( "Unable to set video mode: %s\n", SDL_GetError());
    exit(1);
  }
  
  //SDL_ShowCursor(SDL_DISABLE);
  
  font=TTF_OpenFont(TEXT_FONT,TEXT_SIZE);
  if (font==NULL)
  {
    printf( "Unable to open font: %s\n", TTF_GetError());
    exit(1);
  }
  
	load_graphics();
	
	srand((int)time(0));
	
	new_level();
	load_level();
  
  running=1;
  go=0;
  mode=MODE_PLAY;
  
  #if EVENT_THREAD
    ev_thread=SDL_CreateThread(event_handler_thread,NULL);
  #endif
  
  while (running)
  {
    ticks=SDL_GetTicks();
    
    #if !EVENT_THREAD
      while(SDL_PollEvent(&event))
      {
        event_handler(event);
      }
    #endif
    
    if ((ticks-tprev)>(1000/(MAX_FPS)))
    {
      fps_avg=0;
      for (i=0;i<(TEXT_FPS_ACC-1);i++)
      {
        fps_hist[i]=fps_hist[i+1];
        fps_avg+=fps_hist[i];
      }
      fps_hist[TEXT_FPS_ACC-1]=(1000)/(ticks-tprev);
      fps_avg+=fps_hist[TEXT_FPS_ACC-1];
      fps=fps_avg/TEXT_FPS_ACC;
      
      do_cycle();
      
      draw_main();
      tprev=ticks;
    }
    else
    {
      SDL_Delay(0);
    }
  }
  
  #if EVENT_THREAD
    SDL_KillThread(ev_thread);
  #endif
  
  //if (SDL_GetError()) {printf("Err: %s\n", SDL_GetError());}
  
  exit(0);
}

void clean_up()
{
  TTF_CloseFont(font);

  TTF_Quit();
  SDL_Quit();
}


