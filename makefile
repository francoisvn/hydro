CC =		gcc

CFLAGS =	-O3 -Wall $(shell sdl-config --cflags) -DEVENT_THREAD=1
CLIBS =		$(shell sdl-config --libs) -lSGE -lSDL_ttf -lfreetype

WINCC =		i586-mingw32msvc-gcc -O3 -Wall -DEVENT_THREAD=0
WINCLIBS =		-I/usr/i586-mingw32msvc/include -I/usr/i586-mingw32msvc/include/SDL -D_GNU_SOURCE=1 -Dmain=SDL_main -L/usr/i586-mingw32msvc/lib -lmingw32 -lSDLmain -lSDL -lSGE -lSDL_image -lSDL_ttf -mwindows

SOURCES =	hydro.c hydro.h

all:		hydro

clean:
		rm hydro

hydro:	${SOURCES}
		${CC} ${CFLAGS} -o hydro ${SOURCES} ${CLIBS}

win:	${SOURCES}
		${WINCC} -o hydro.exe ${SOURCES} ${WINCLIBS}

 
